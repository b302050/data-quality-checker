#!/bin/bash

## Scripts needs a bach version which supports globstar

set -e

if (( $(echo ${BASH_VERSION:0:3} | sed 's/\.//g')  <= 40 ))
then
	echo "This script needs bash version 4 and higher"
	echo "Your bach verison is: ${BASH_VERSION:0:3}"
	exit 1
fi

source $SHLIB/config.shlib
source $SHLIB/parallel_open_semaphore.shlib
source $SHLIB/parallel_run_with_lock.shlib

shopt -s globstar || echo "WARNING: globstar is not set. Globstar need bash v4."

#basedir="/e/uwork1/kpankatz/miklip/preoperational/output/DWD/MPI-ESM-HR"
basedir=$(config_get basedir)

[[ -d $basedir ]] || { echo "ERROR: $basedir does not exist" && exit 1; }

if (( $# != 1 ))
then
	echo "Skript needs exactly one argument"
	exit 1	
fi

year=$1

N_parallel_procs=$(config_get N_parallel_procs)
open_semaphore $N_parallel_procs || { echo "$0: Could not open semaphore"; exit 1; }

function check_ntimes {

	#echo $file
	ntimes=$(cdo -s ntime $file || echo $file)
	# for time_freq 6hr there are 11 files
	# ten files contain a whole year worth of data
	# one file, the first year, contains just november and december
	if [[ ${time_freq##*/} == 6hr ]]
	then
		actual_year=${file##*_}
		actual_year=${actual_year:0:4}
	#echo $actual_year
		if (( $year == $actual_year ))
		then
			ntimes_allowed=244
		else
			ntimes_allowed=$(echo "( $(date -d ${actual_year}1231 +%s) - $(date -d ${actual_year}0101 +%s)) / (24*900) + 4  " | bc ) # plus 4: Alleebaum-Problem
		fi
	fi
	if (( $ntimes != $ntimes_allowed ))
	then
		echo $file: $ntimes expected $ntimes_allowed
	fi
}

for year in $(seq $year $year)
do
	echo "Checking year $year"
	if [[ -d $basedir/dcppA$year ]] && (( $(ls -d $basedir/dcppA$year/* | wc -l) > 0 ))
	then
		for time_freq in $basedir/dcppA$year/*
		do
			# Depending on the time_frequency, ntimes and nfiles can be determined
			#echo $time_freq
			if [[ ${time_freq##*/} == 6hr ]] 
			then
				nfiles_allowed=11
			elif [[ ${time_freq##*/} == mon ]] || [[ ${time_freq##*/} == day ]]
			then
				nfiles_allowed=1
			else
				echo "ERROR: nfiles_allowed not set. This should not occur"
				exit 1
			fi
			if [[ ${time_freq##*/} == day ]]
			then
				ntimes_allowed=$(echo "( $(date -d $((year + 10))1231 +%s) - $(date -d ${year}1101 +%s)) / (24*3600) + 1" | bc )
			elif [[ ${time_freq##*/} == mon ]]
			then
				ntimes_allowed=122
			fi

			for realm in $time_freq/*
			do
				#echo $realm
				for variable in $realm/*
				do
					#echo $variable
					for member in $variable/*
					do
						#echo $member
						nfiles=$(ls $member | wc -l)
						# 11 should be the default for a decadal prediction
						# Skip if nfiles equals zero
						if (( $nfiles == 0 ))
						then
							continue
						elif (( $nfiles != $nfiles_allowed )) 
						then
							# This warning by itself is a result
							# Don't need to check content of files then
							echo "WARNING: $member contains $nfiles" 
							continue
						fi	
						# And determined number of timesteps per file
						for file in $member/*
						do
							run_with_lock check_ntimes
						done
					done
				done
			done
		done
	else
		echo "ERROR: Folder $basedir/dcppA$year missing"
		exit 1
	fi
done
