#!/bin/bash

set -e

if (( $(echo ${BASH_VERSION:0:3} | sed 's/\.//g')  <= 40 ))
then
	echo "This script needs bash version 4 and higher"
	echo "Your bach verison is: ${BASH_VERSION:0:3}"
	exit 1
fi

if (( $# != 1 ))
then
	echo "ERROR: Please supply a variable name"
	exit 1
fi

source $SHLIB/config.shlib

shopt -s globstar

basedir=$(config_get basedir)
[[ ! -d $basedir ]] && echo "ERROR: $basedir does not exist" && exit 1

cd $basedir

variable=$1

echo $variable

allowed_nfiles=5

for dir in */
do 
	#echo $dir
	nfiles=$(ls $dir/**/${variable}_Aday*.nc 2>/dev/null | wc -l)
	if (( $nfiles != $allowed_nfiles )) && (( $nfiles != 10 ))
	then
		echo $dir: $nfiles
	fi	
	# check timesteps also
	year=$(echo ${dir/dcppA} | sed -e "s:/::g")
	ntimes_allowed=$(echo "( $(date -d $((year + 10))1231 +%s) - $(date -d ${year}1101 +%s)) / (24*3600) + 1" | bc ) # plus one Alleebaum-Problem
	for file in $dir/**/${variable}_Aday*.nc
	do
		ntimes=$(cdo -s ntime $file)
		if (( $ntimes != $ntimes_allowed ))
		then
			echo $file: $ntimes expected $ntimes_allowed
		fi
	done
done

