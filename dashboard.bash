#!/bin/bash


source $SHLIB/config.shlib

basedir=$(config_get basedir)
! [[ -d $basedir ]] && echo "ERROR: $basedir does not exist" && exit 1
year=$1

./check_number_of_files_CMOR.bash $year &

# What is "remote" depends on where you are
# remotes: lce, mistral, cca, 713
# if on mistral dwd servers are out of reach

for i in $(seq 6 10)
do 
	if [[ $HOSTNAME =~ mlogin* ]]
	then
		echo r${i}: mistral $(find $basedir/dcppA${year}/ -name "*_r${i}*" | wc -l)
	else
		echo r${i}: lce $(find $basedir/dcppA${year}/ -name "*_r${i}*" | wc -l) mistral $(ssh mistral find /work/bmx826/global/prod/archive/preop/output/DWD/MPI-ESM-HR/dcppA${year}/ -name "*_r${i}*" | wc -l)
	fi
done
echo "Waiting"
wait

# run check script
# count cmoroized files on lce
# count cmorized fiels on mistral


